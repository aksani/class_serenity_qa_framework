package core.api.utils;

import com.google.gson.Gson;
import core.api.pojo.Pojo;

public class JsonUtils {

    private JsonUtils() {
    }

    public static String generateJsonFromPojo(final Pojo pojoObject){
        Gson gson = new Gson();
        return gson.toJson(pojoObject);
    }

}
