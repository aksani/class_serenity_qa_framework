package core.api;


import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import static io.restassured.RestAssured.given;


public class HttpMethods {

    private String userAgent;
    private String cookie;
    private WebDriver driver;

    public HttpMethods(final WebDriver driver, final String baseUrl){
        this.driver = driver;
        RestAssured.baseURI = baseUrl;
    }


    public ValidatableResponse post(final String url, final String body) {
        initUserAgentAndCookies();
        ValidatableResponse response = given().
                header("Content-Type", ContentType.JSON).
                header("User-Agent", userAgent).
                //TODO
                cookie("Cookie","JSESSIONID=" + driver.manage().getCookieNamed("JSESSIONID")).
                body(body).
                when().
                post(url).
                then();
        System.out.println(response.extract().statusCode());
        return response;
    }


   /* public void get(final String uri){
        initUserAgentAndCookies();
        final Client client = new Client();
        client.resource(uri);

    }*/

   private void initUserAgentAndCookies(){
       this.cookie = String.valueOf(((JavascriptExecutor) driver).executeScript("return document.cookie"));
       this.userAgent = String.valueOf(((JavascriptExecutor) driver).executeScript("return navigator.userAgent"));
   }

}
