package core.api.pojo;

import java.util.List;

public class ItemEntries implements Pojo{

    private List<Product> itemEntries;

    public ItemEntries(List<Product> itemEntries) {
        this.itemEntries = itemEntries;
    }

    public List<Product> getItemEntries() {
        return itemEntries;
    }

    public void setItemEntries(final List<Product> itemEntries) {
        this.itemEntries = itemEntries;
    }
}
