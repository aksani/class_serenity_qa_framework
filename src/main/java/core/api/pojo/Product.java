package core.api.pojo;

public class Product implements Pojo {

    private String code;
    private String productPageUrl;
    private String productPictureUrl;
    private String quantity;


    public Product(final String code, final String productPageUrl, final String productPictureUrl,
                   final String quantity) {
        this.code = code;
        this.productPageUrl = productPageUrl;
        this.productPictureUrl = productPictureUrl;
        this.quantity = quantity;
    }

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getProductPageUrl() {
        return productPageUrl;
    }

    public void setProductPageUrl(final String productPageUrl) {
        this.productPageUrl = productPageUrl;
    }

    public String getProductPictureUrl() {
        return productPictureUrl;
    }

    public void setProductPictureUrl(final String productPictureUrl) {
        this.productPictureUrl = productPictureUrl;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(final String quantity) {
        this.quantity = quantity;
    }

}
