package core.common.pages;

import core.common.panels.HeaderPanel;
import core.common.panels.ProductOptionsPanel;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class ProductDetailsPage extends AbstractPage{

    private static final String PAGE_HEADER_LOCATOR = "//header[@data-module-type='Header']";
    private static final String PRODUCT_OPTIONS_PANEL_LOCATOR = "//div[@class='product-detail__options']";


    public ProductDetailsPage(WebDriver driver) {
        super(driver);
    }

    public HeaderPanel getHeaderPanel(){
        return new HeaderPanel(withTimeoutOf(5, TimeUnit.SECONDS).find(PAGE_HEADER_LOCATOR), this);
    }

    public ProductOptionsPanel getProductOptionsPanel(){
        return new ProductOptionsPanel(withTimeoutOf(5, TimeUnit.SECONDS).find(PRODUCT_OPTIONS_PANEL_LOCATOR), this);
    }

}
