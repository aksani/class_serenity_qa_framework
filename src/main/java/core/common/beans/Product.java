package core.common.beans;

import core.annotations.TableFieldName;


public class Product {

    @TableFieldName("title")
    private String title;

    @TableFieldName("price")
    private String price;

    @TableFieldName("size")
    private String size;

    @TableFieldName("qty")
    private String qty;

    @TableFieldName("total")
    private String total;

    public Product(){}

    public Product(final String title, final String price, final String size, final String qty, final String total) {
        this.title = title;
        this.price = price;
        this.size = size;
        this.qty = qty;
        this.total = total;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(final String price) {
        this.price = price;
    }

    public String getSize() {
        return size;
    }

    public void setSize(final String size) {
        this.size = size;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(final String qty) {
        this.qty = qty;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(final String total) {
        this.total = total;
    }
}
