package core.common.panels;

import core.common.pages.AbstractPage;
import core.common.utils.WebDriverUtil;
import net.serenitybdd.core.pages.WebElementFacade;

public class HeaderPanel extends AbstractPanel {



    private static final String SHOPPING_CART_BTN_LOCATOR = ".//a[@class='header-cart__trigger drawer-ui__toggle']";

    public HeaderPanel(final WebElementFacade panelBaseLocation, final AbstractPage driverDelegate) {
        super(panelBaseLocation, driverDelegate);
    }

    public void clickShoppingCartBtn(){
        final WebElementFacade shoppingCartButton = getDriverDelegate().findBy(SHOPPING_CART_BTN_LOCATOR);
        shoppingCartButton.then().click();
        WebDriverUtil.waitForPageLoad();
    }
}
