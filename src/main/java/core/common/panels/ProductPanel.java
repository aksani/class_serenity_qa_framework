package core.common.panels;

import core.common.pages.AbstractPage;
import net.serenitybdd.core.pages.WebElementFacade;

public class ProductPanel extends AbstractPanel {

    private static final String TITLE_LOCATOR = ".//a[@class='sc-product__title-link']";
    private static final String SIZE_LOCATOR =
            ".//span[@class='sc-product__property__value' and ./span[contains(text(),'Size')]]";
    private static final String QTY_LOCATOR = ".//input[@class='sc-product__qty']";
    private static final String PRICE_LOCATOR = ".//span[@class='sc-product__price']";
    private static final String TOTAL_LOCATOR = ".//div[@class='sc-product__column sc-product__column_total']//span";

    public ProductPanel(final WebElementFacade panelBaseLocation, final AbstractPage driverDelegate) {
        super(panelBaseLocation, driverDelegate);
    }


    public String getTitle() {
        return getDriverDelegate().findBy(TITLE_LOCATOR).getText();
    }

    public String getSize() {
        return getDriverDelegate().findBy(SIZE_LOCATOR).getText();
    }

    public String getQty() {
        return getDriverDelegate().findBy(QTY_LOCATOR).getValue();
    }

    public String getPrice() {
        return getDriverDelegate().findBy(PRICE_LOCATOR).getText();
    }

    public String getTotal() {
        return getDriverDelegate().findBy(TOTAL_LOCATOR).getText();
    }
}
