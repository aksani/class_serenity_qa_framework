package core.common.panels;

import core.common.pages.AbstractPage;
import core.common.webdriver.WebDriverAdaptor;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.locators.SmartElementLocatorFactory;
import net.thucydides.core.annotations.locators.SmartFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.FieldDecorator;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public abstract class AbstractPanel {


    private long waitForTimeoutInMilliseconds;
    private AbstractPage driverDelegate;
    private WebDriverAdaptor panelToWebDriver;

    protected AbstractPanel(final WebElementFacade panelBaseLocation, final AbstractPage driverDelegate) {
        initPanel(panelBaseLocation, driverDelegate);

    }

    private void initPanel(final WebElementFacade panelBaseLocation, final AbstractPage driverDelegate) {

        this.driverDelegate = driverDelegate;
        waitForTimeoutInMilliseconds = driverDelegate.waitForTimeoutInMilliseconds();
        panelToWebDriver = new WebDriverAdaptor(panelBaseLocation, getDriver());
        final ElementLocatorFactory finder = new SmartElementLocatorFactory(panelToWebDriver, null,
                (int) getTimeoutInSeconds());
        final FieldDecorator decorator = new SmartFieldDecorator(finder, getDriver(), driverDelegate);
        PageFactory.initElements(decorator, this);
    }

    private long getTimeoutInSeconds(){
        return (waitForTimeoutInMilliseconds < 1000) ? 1 : (waitForTimeoutInMilliseconds / 1000);
    }

    protected  AbstractPage getDriverDelegate() {
        return driverDelegate;
    }

}
