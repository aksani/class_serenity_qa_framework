package core.common.panels;

import core.common.pages.AbstractPage;
import core.common.utils.WebDriverUtil;
import net.serenitybdd.core.pages.WebElementFacade;

public class ProductOptionsPanel extends AbstractPanel {

    private static final String SIZE_SELECT_LOCATOR = ".//select[@data-control-type='size']";
    private static final String QTY_SELECT_LOCATOR = ".//select[@name='quantity']";
    private static final String ADD_TO_CART_BTN_LOCATOR = ".//button[contains(@class, 'add-cart')]";

    public ProductOptionsPanel(final WebElementFacade panelBaseLocation, final AbstractPage driverDelegate) {
        super(panelBaseLocation, driverDelegate);
    }

    public void selectSize(final String size) {
        getDriverDelegate().findBy(SIZE_SELECT_LOCATOR).selectByVisibleText(size);
        WebDriverUtil.waitForPageLoad();
    }

    public void selectQty(final String qty) {
        getDriverDelegate().findBy(QTY_SELECT_LOCATOR).selectByVisibleText(qty);
        WebDriverUtil.waitForPageLoad();
    }

    public void clickAddToCartBtn() {
        final WebElementFacade addToCartBtn = getDriverDelegate().findBy(ADD_TO_CART_BTN_LOCATOR);
        addToCartBtn.then().click();
        WebDriverUtil.waitForPageLoad();
    }
}
