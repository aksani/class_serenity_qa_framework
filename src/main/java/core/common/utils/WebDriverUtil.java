package core.common.utils;

import core.common.pages.AbstractPage;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.webdriver.ThucydidesWebDriverSupport;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Optional;

public class WebDriverUtil {

    private static String scriptToCheck;


    private WebDriverUtil(){
    }

    public static WebDriver getDriver(){
        return ThucydidesWebDriverSupport.getDriver();
    }

    public static <T extends AbstractPage> String getDefaultUrl(final Class<T> page) {
        return Optional.ofNullable(page)
                .map(pageItem -> pageItem.getAnnotation(DefaultUrl.class))
                .map(DefaultUrl::value)
                .orElse(null);
    }


    public static void waitForPageLoad(){
        scriptToCheck = "return ((typeof jQuery !== 'undefined') && (jQuery.active == 0))";
        WebDriverWait wait = new WebDriverWait(getDriver(), 30);
        wait.until(e -> {
            final Object scriptResult = ((JavascriptExecutor) getDriver()).executeScript(scriptToCheck);
            return Boolean.parseBoolean(String.valueOf(scriptResult));
        });
    }
}
