package core.common.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyUtils {

    private static final String PROPERTIES_FILE_PATH = "src/main/resources/environment/properties/qa.properties";
    private Properties properties = new Properties();

    public PropertyUtils() {
        readProperties();
    }

    private void readProperties(){
        try (InputStream inputStream = new FileInputStream(PROPERTIES_FILE_PATH)){
            properties.load(inputStream);
        } catch (IOException e) {
            throw new IllegalStateException("Couldn't read file!", e);
        }
    }

    public String getProperty(final String propertyKey) {
        return properties.getProperty(propertyKey);
    }



}
