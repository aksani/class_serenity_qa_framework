package core.common.utils;

import core.annotations.TableFieldName;
import org.jbehave.core.model.ExamplesTable;
import org.jbehave.core.steps.Row;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ExamplesTableUtil {

    private ExamplesTableUtil() {
    }

    public static <T> List<T> convert(final ExamplesTable exampleTable, final Class<T> targetObjectClass) {
        List<T> listOfObjects = new ArrayList<>();
        List<Map<String, String>> tableValues =  exampleTable.getRowsAsParameters(true).stream()
                .map(Row::values).collect(Collectors.toList());
        for (Map<String, String> rowMap : tableValues){
            T objectInstance = null;
            try {
                objectInstance = targetObjectClass.newInstance();
                for (Map.Entry<String, String> mapElement : rowMap.entrySet()){
                    Field targetField = Arrays.stream(targetObjectClass.getDeclaredFields())
                            .filter(field -> field.getAnnotation(TableFieldName.class).value().equals(mapElement.getKey()))
                            .findAny().get();
                    targetField.setAccessible(true);
                    targetField.set(objectInstance, mapElement.getValue());
                }
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
            listOfObjects.add(objectInstance);
        }
        return listOfObjects;
    }
}
