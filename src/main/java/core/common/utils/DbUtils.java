package core.common.utils;

import java.sql.*;
import java.util.*;

public class DbUtils {

    private static PropertyUtils propertyUtils = new PropertyUtils();
    private static final String CONNECTION_STRING = propertyUtils.getProperty("mysql.connection.string");
    private static final String DB_LOGIN_STRING = propertyUtils.getProperty("mysql.login");
    private static final String DB_PASSWD_STRING = propertyUtils.getProperty("mysql.password");

    private Connection connection;
    private ResultSet resultSet;

    public DbUtils() {
        this.connection = openConnection();
        try {
            if (Objects.nonNull(connection) && !connection.isClosed()){
                System.out.println("Connection has been established!");
            }
        } catch (SQLException e) {
            throw new IllegalStateException("Database connection failed!", e);
        }
    }

    public Connection openConnection(){
        try {
            return DriverManager.getConnection(CONNECTION_STRING, DB_LOGIN_STRING, DB_PASSWD_STRING);
        } catch (SQLException e) {
            throw new IllegalStateException("Database connection failed!", e);
        }
    }

    public Connection getConnection(){
        return this.connection;
    }


    public static void main(String[] args) throws SQLException {
        DbUtils dbUtils = new DbUtils();
        Statement statement = dbUtils.getConnection().createStatement();
        /*int rowsCount = statement.executeUpdate("insert into authors (first_name, last_name, email, birthdate)" +
                                                    "values('Vasya', 'Vasiliev', 'vasya@mail.com', '1980-06-20');");*/

        ResultSet resultSet = statement.executeQuery("select * from authors;"/*"SHOW CREATE TABLE authors;"*/);
        dbUtils.parseResult(resultSet)
                .forEach(e -> e.
                        forEach((key, value) -> System.out.printf("%s : %s\n", key, value)));

    }

    private List<Map<String, Object>> parseResult(final ResultSet resultSet) throws SQLException {
        List<Map<String, Object>> dbResult = new ArrayList<>();
        while (resultSet.next()) {
            Map<String, Object> row = new HashMap<>();
            for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++){
                row.put(resultSet.getMetaData().getColumnName(i), resultSet.getObject(i));
            }
            dbResult.add(row);
        }
        return dbResult;
    }
}
