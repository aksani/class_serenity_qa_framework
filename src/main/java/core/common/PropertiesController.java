package core.common;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PropertiesController {

    private static PropertiesController instance;
    private final Properties properties = new Properties();
    private static final String PROPERTY_FILE_DEFAULT_PATH = "/src/main/resources/environment/properties/";
    private static final String ENV_CONFIG_FILE = "env.config.file";
    private static final String PROPERTY_TEMPLATE = "\\$\\{([a-zA-Z0-9]+(?:\\.[a-zA-Z0-9]+)*+)}";

    private PropertiesController() {
        final String envConfigFile = System.getProperty(ENV_CONFIG_FILE);
        final String path = PROPERTY_FILE_DEFAULT_PATH + envConfigFile;
        properties.put(ENV_CONFIG_FILE, path);
    }

    private static PropertiesController getInstance(){
        return Objects.isNull(instance) ? instance = new PropertiesController() : instance;
    }

    public static String getProperty(final String propertyName){
        String propertyValue = System.getProperty(propertyName, getInstance().properties.getProperty(ENV_CONFIG_FILE));
        if (Objects.nonNull(propertyName) && Pattern.matches(PROPERTY_TEMPLATE, propertyValue)) {
            final Matcher m = Pattern.compile(PROPERTY_TEMPLATE).matcher(propertyValue);
            while (m.find()) {
                final String valueToReplace = m.group(1);
                propertyValue = StringUtils.replaceOnce(propertyValue, m.group(0), getProperty(valueToReplace));
            }
        }
    }
}
