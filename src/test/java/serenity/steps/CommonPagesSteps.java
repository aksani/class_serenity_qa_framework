package serenity.steps;

import core.api.HttpMethods;
import core.api.pojo.ItemEntries;
import core.api.pojo.Product;
import core.api.utils.JsonUtils;
import core.common.pages.HomePage;
import net.thucydides.core.annotations.Step;

import java.util.Arrays;

public class CommonPagesSteps {

    HomePage homePage;

    @Step
    public void openHomePage() {
        homePage.open();
    }

    @Step
    public void sendPost(final String queryUrl) {
        Product product = new Product("332340187",
                "https://www.sportchek.ca/categories/men/footwear/hiking-outdoor-shoes/multi-sport/product/salomon-mens-pathfinder-waterproof-hiking-shoes-phantomblack-332340172.html#332340172=332340187",
                "//fgl.scene7.com/is/image/FGLSportsLtd/332340172_04_a-Salomon-Mens-Pathfinder-Waterproof-Hiking-Shoes-Phantom-Black-L40061400",
                "1");
        ItemEntries itemEntries = new ItemEntries(Arrays.asList(product));
        String json = JsonUtils.generateJsonFromPojo(itemEntries);
        HttpMethods httpMethods = new HttpMethods(homePage.getDriver(), "https://www.sportchek.ca/");
        httpMethods.post(queryUrl, json);
    }
}
