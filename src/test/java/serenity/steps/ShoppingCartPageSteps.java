package serenity.steps;

import core.common.beans.Product;
import core.common.pages.ShoppingCartPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

public class ShoppingCartPageSteps extends ScenarioSteps{

    private ShoppingCartPage shoppingCartPage;

    public ShoppingCartPageSteps(final Pages pages) {
        shoppingCartPage = pages.getPage(ShoppingCartPage.class);
    }

    @Step
    public Product getProductDetails(){
        return new Product(getTitle(), getPrice(), getSize(), getQty(), getTotal());
    }

    @Step
    public String getTitle() {
        return shoppingCartPage.getProductPanel().getTitle();
    }

    @Step
    public String getSize() {
        return shoppingCartPage.getProductPanel().getSize();
    }

    @Step
    public String getQty() {
        return shoppingCartPage.getProductPanel().getQty();
    }

    @Step
    public String getPrice() {
        return shoppingCartPage.getProductPanel().getPrice();
    }

    @Step
    public String getTotal() {
        return shoppingCartPage.getProductPanel().getTotal();
    }

}
