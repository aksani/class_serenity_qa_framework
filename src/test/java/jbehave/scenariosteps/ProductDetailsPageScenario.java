package jbehave.scenariosteps;

import core.common.beans.Product;
import core.common.utils.ExamplesTableUtil;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import serenity.steps.HomePageSteps;
import serenity.steps.ProductDetailsPageSteps;

public class ProductDetailsPageScenario {

    @Steps
    private HomePageSteps homePageSteps;

    @Steps
    private ProductDetailsPageSteps productDetailsPageSteps;


    @Given("user has opened product details page: $partialUrl")
    public void navigateToProductDetailsPage(final String partialUrl){
        homePageSteps.openProductDetailsPage(partialUrl);
    }

    @When("user fill in following parameters: $productParameters")
    public void fillInProductParameters(final ExamplesTable parameters){
        for (Product product : ExamplesTableUtil.convert(parameters, Product.class)) {
            productDetailsPageSteps.setSize(product.getSize());
            productDetailsPageSteps.setQty(product.getQty());
        }
    }

    @When("clicks 'ADD TO CART' button")
    public void clickAddToCartButton(){
        productDetailsPageSteps.clickAddToCartButton();
    }

}
