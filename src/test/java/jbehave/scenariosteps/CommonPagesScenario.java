package jbehave.scenariosteps;

import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;
import serenity.steps.CommonPagesSteps;
import serenity.steps.ProductDetailsPageSteps;

public class CommonPagesScenario {

    @Steps
    private ProductDetailsPageSteps productDetailsPageSteps;

    @Steps
    private CommonPagesSteps commonPagesSteps;


    @Given("user has opened Home Page")
    public void navigateToHomePage(){
        commonPagesSteps.openHomePage();
    }

    @When("user sends post on URL: $queryUrl")
    public void sendPostOnUrl(final String queryUrl) {
        commonPagesSteps.sendPost(queryUrl);
    }

    @When("navigate to Shopping Cart Page")
    public void navigateToShoppingCart(){
        productDetailsPageSteps.navigateToShoppingCart();
    }

}
