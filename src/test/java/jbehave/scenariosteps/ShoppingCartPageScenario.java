package jbehave.scenariosteps;

import core.common.beans.Product;
import core.common.utils.ExamplesTableUtil;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.model.ExamplesTable;
import org.unitils.reflectionassert.ReflectionAssert;
import serenity.steps.ShoppingCartPageSteps;


public class ShoppingCartPageScenario {

    @Steps
    private ShoppingCartPageSteps shoppingCartPageSteps;

    @Then("following product should be added to shopping cart: $shoppingCartItemParameters")
    public void checkShoppingCartItems(final ExamplesTable shoppingCartItemParameters){
        Product product = ExamplesTableUtil.convert(shoppingCartItemParameters, Product.class).get(0);
        ReflectionAssert.assertLenientEquals(product, shoppingCartPageSteps.getProductDetails());
    }
}
