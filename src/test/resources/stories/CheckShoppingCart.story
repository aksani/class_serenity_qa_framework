Meta:
@ShoppingCart

Narrative:
In order to buy a product
As a user
I want to be able to see saleable product added to Shopping Cart Page

Scenario: Check add product to Shopping Cart

Given user has opened product details page: /categories/men/footwear/training-shoes/product/adidas-mens-athletics-247-training-shoes-dark-greywhite-332282849.html
When user fill in following parameters:
|size|qty|
|size|qty|
And clicks 'ADD TO CART' button
And navigate to Shopping Cart Page
Then following product should be added to shopping cart:
|title                                                       |qty|price |total  |
|adidas Men's Athletics 24/7 Training Shoes - Dark Grey/White|2  |$83,96|$162,92|

Examples:
|size|qty|
|8   |1  |
|10  |3  |
|13  |5  |



Scenario: Check add item to shopping cart through api

Given user has opened Home Page
When user sends post on URL: /services/sportchek/cart/entry
And navigate to Shopping Cart Page
Then following product should be added to shopping cart:
|title                                                           |qty|price  |total |
|Salomon Men's Pathfinder Waterproof Hiking Shoes - Phantom/Black|1  |$159.99|$99.99|